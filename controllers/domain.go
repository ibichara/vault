package controllers

import (
	"errors"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"gitlab.com/ibichara/vault/models"
)

type CreateDomainInput struct {
	Name string `json:"name" binding:"required"`
}

type UpdateDomainInput struct {
	Name string `json:"name"`
}

func FindDomains(c *gin.Context) {
	var domains []models.Domain
	models.DB.Find(&domains)

	c.JSON(http.StatusOK, gin.H{"data": domains})
}

func FindDomain(c *gin.Context) {
	var domain models.Domain
	result := models.DB.Where("id=?", c.Param("id")).First(&domain)
	if result.Error != nil {
		statusCode := http.StatusInternalServerError
		if errors.Is(result.Error, gorm.ErrRecordNotFound) {
			statusCode = http.StatusNotFound
		}
		c.JSON(statusCode, gin.H{"error": result.Error.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": domain})
}

func CreateDomain(c *gin.Context) {
	var input CreateDomainInput
	err := c.ShouldBindJSON(&input)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	domain := models.Domain{Name: input.Name}
	models.DB.Create(&domain)

	c.JSON(http.StatusOK, gin.H{"data": domain})
}

func UpdateDomain(c *gin.Context) {
	var domain models.Domain
	result := models.DB.Where("id=?", c.Param("id")).First(&domain)
	if result.Error != nil {
		statusCode := http.StatusInternalServerError
		if errors.Is(result.Error, gorm.ErrRecordNotFound) {
			statusCode = http.StatusNotFound
		}
		c.JSON(statusCode, gin.H{"error": result.Error.Error()})
		return
	}

	var input UpdateDomainInput
	err := c.ShouldBindJSON(&input)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	models.DB.Model(&domain).Updates(input)

	c.JSON(http.StatusOK, gin.H{"data": domain})
}

func DeleteDomain(c *gin.Context) {
	var domain models.Domain
	result := models.DB.Where("id=?", c.Param("id")).First(&domain)
	if result.Error != nil {
		statusCode := http.StatusInternalServerError
		if errors.Is(result.Error, gorm.ErrRecordNotFound) {
			statusCode = http.StatusNotFound
		}
		c.JSON(statusCode, gin.H{"error": result.Error.Error()})
		return
	}

	models.DB.Delete(domain)

	c.JSON(http.StatusOK, gin.H{"data": true})
}
