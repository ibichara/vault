package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/ibichara/vault/controllers"
	"gitlab.com/ibichara/vault/models"
)

func main() {
	router := gin.Default()
	models.ConnectDatabase()

	router.GET("/", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{"healthcheck": "ok"})
	})

	router.POST("/domain", controllers.CreateDomain)
	router.PATCH("/domain/:id", controllers.UpdateDomain)
	router.DELETE("/domain/:id", controllers.DeleteDomain)
	router.GET("/domain", controllers.FindDomains)
	router.GET("/domain/:id", controllers.FindDomain)

	router.Run()
}
